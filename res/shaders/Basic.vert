#version 330
in vec2 inPosition;
in vec3 inColor;

out vec3 vertColor;

void main() {
    vertColor = inColor;

    gl_Position = vec4(inPosition, 0.f, 1.f);
}