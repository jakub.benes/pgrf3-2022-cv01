import lwjglutils.ShaderUtils;

import static org.lwjgl.opengl.GL33.*;

public class Renderer {
    public Renderer() {
        // Shaders
        int shaderProgram = ShaderUtils.loadProgram("/shaders/Basic");
        glUseProgram(shaderProgram);

        // Vertex: position (2 x float), color (3 x float)
        float[] vertices = {
                -1.f, -1.f,     1.f, 0.f, 0.f, // 1
                 1.f,  0.f,     0.f, 1.f, 0.f, // 2
                 0.f,  1.f,     0.f, 0.f, 1.f, // 3
        };

        // Indices: TRIANGLES
        int[] indices = {
                0, 1, 2
        };

        // OpenGL: vertex buffer
        int vb = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vb);
        glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);

        // OpenGL: index buffer
        int ib = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, GL_STATIC_DRAW);

        // Positions
        int loc_inPosition = glGetAttribLocation(shaderProgram, "inPosition");
        glVertexAttribPointer(loc_inPosition, 2, GL_FLOAT, false, 5 * Float.BYTES, 0);
        glEnableVertexAttribArray(loc_inPosition);
        // Color
        int loc_inColor = glGetAttribLocation(shaderProgram, "inColor");
        glVertexAttribPointer(loc_inColor, 3, GL_FLOAT, false, 5 * Float.BYTES, 8);
        glEnableVertexAttribArray(loc_inColor);
    }

    public void display()
    {
        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, 0);
    }
}
